
import { NgModule } from "@angular/core";
import { MatMenuModule } from '@angular/material/menu';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTabsModule } from '@angular/material/tabs';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatTableModule } from '@angular/material/table';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatDialogModule } from '@angular/material/dialog';

@NgModule({
    imports: [
        MatMenuModule,
        MatSidenavModule,
        MatToolbarModule,
        MatTabsModule,
        MatListModule,
        MatIconModule,
        MatButtonModule,
        MatFormFieldModule,
        MatSelectModule,
        MatInputModule,
        MatCardModule,
        MatDividerModule,
        MatTableModule,
        MatDatepickerModule,
        MatGridListModule,
        MatPaginatorModule,
        MatDialogModule
    ],
    exports: [
        MatMenuModule,
        MatSidenavModule,
        MatToolbarModule,
        MatTabsModule,
        MatListModule,
        MatIconModule,
        MatButtonModule,
        MatFormFieldModule,
        MatSelectModule,
        MatInputModule,
        MatCardModule,
        MatDividerModule,
        MatTableModule,
        MatDatepickerModule,
        MatGridListModule,
        MatPaginatorModule,
        MatDialogModule
    ]
})
export class CustomMaterialModule { }
