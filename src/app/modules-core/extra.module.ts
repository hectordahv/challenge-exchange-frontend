
import { NgModule } from "@angular/core";
import { NgxSpinnerModule } from "ngx-spinner";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [
      NgxSpinnerModule,
      FormsModule,
      ReactiveFormsModule,
    ],
    exports: [
      NgxSpinnerModule,
      FormsModule,
      ReactiveFormsModule,
    ]
})
export class ExtraModule { }
