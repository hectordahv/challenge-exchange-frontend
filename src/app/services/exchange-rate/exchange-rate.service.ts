import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Currency } from 'src/app/interfaces/currency';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { ExchangeRate } from 'src/app/interfaces/ExchangeRate';

@Injectable({
  providedIn: 'root',
})
export class ExchangeRateService {
  constructor(private http: HttpClient) {}

  private getToken() {
    return {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    };
  }

  public find(currencyFrom: string, currencyTo: string): Observable<ExchangeRate> {
    return this.http.post<ExchangeRate>(
      environment.URL_API + '/exchange-rate/find',
      { currencyFrom, currencyTo },
      this.getToken()
    );
  }
}
