import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { Exchange } from 'src/app/interfaces/Exchange';

@Injectable({
  providedIn: 'root',
})
export class ExchangeService {
  constructor(private http: HttpClient) {}

  private getToken() {
    return {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    };
  }

  public create(currencyFrom: string, currencyTo: string, amount: number): Observable<Exchange> {
    return this.http.post<Exchange>(
      environment.URL_API + '/exchange/create',
      { currencyFrom, currencyTo, amount },
      this.getToken()
    );
  }

  public list(): Observable<Exchange[]> {
    return this.http.get<Exchange[]>(
      environment.URL_API + '/exchange/list',
      this.getToken()
    );
  }
}
