import { Injectable } from '@angular/core';
import { Credentials } from 'src/app/interfaces/credentials';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  public login(credentials: Credentials): Observable<any> {
    return this.http.post<any>(environment.URL_API + '/auth/login', credentials);
  }

}
