import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Credentials } from 'src/app/interfaces/credentials';
import { AuthService } from 'src/app/services/auth/auth.service';
import Swal from 'sweetalert2';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;
  public submitted: boolean = false;
  public credentials: Credentials;
  public hide: boolean;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private spinner: NgxSpinnerService
  ) {
    this.loginForm = this.formBuilder.group({
      userName: ['', Validators.required],
      password: ['', Validators.required],
    });
    this.credentials = { userName: '', password: '' };
    this.hide = false;
   }

  ngOnInit(): void {
  }

  public async signIn() {
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    this.spinner.show();
    try {
      const { userName, password } = this.loginForm.value;
      this.credentials = { userName, password };
      const respLogin = await this.authService
        .login(this.credentials).pipe(first()).toPromise();
        console.log(respLogin);
      localStorage.setItem('token', respLogin.token);
      localStorage.setItem('user', respLogin.userName);
      localStorage.setItem('authorities', JSON.stringify(respLogin.authorities));

      this.router.navigate(['home']);
    } catch (error) {
      console.log(error);
      switch ((error as {status: number}).status) {
        case 401:
          Swal.fire({
            title: 'Ingreso inválido',
            text: 'Usuario o contraseña incorrectos',
            icon: 'error',
            confirmButtonText: 'Ok',
          });
          break;
        default:
          Swal.fire({
            title: 'Ha ocurrido algo inesperado',
            text: 'Comuníquese con el administrador',
            icon: 'error',
            confirmButtonText: 'Ok',
          });
          break;
      }
    }
    this.spinner.hide();
  }

}
