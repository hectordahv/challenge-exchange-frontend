import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NavigationComponent } from '../navigation/navigation.component';
import { RegisterComponent } from '../register/register.component';
import { HistorialComponent } from '../historial/historial.component';
//import { ConsultComponent } from '../consult/consult.component';
const routes: Routes = [
  {
    path: '',
    component: NavigationComponent,
    children: [
      { path: '', redirectTo: 'registrar', pathMatch: 'full' },
      { path: 'registrar', component: RegisterComponent },
      //{ path: 'consultar', component: ConsultComponent },
      { path: 'historial', component: HistorialComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
