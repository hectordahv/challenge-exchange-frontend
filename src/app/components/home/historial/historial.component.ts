import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import * as moment from 'moment';
import { NgxSpinnerService } from 'ngx-spinner';
import { Exchange } from 'src/app/interfaces/Exchange';
import { ExchangeService } from 'src/app/services/exchange/exchange.service';

@Component({
  selector: 'app-historial',
  templateUrl: './historial.component.html',
  styleUrls: ['./historial.component.css'],
})
export class HistorialComponent implements OnInit {
  public dataSource: MatTableDataSource<Exchange>;
  public data: Exchange[] = [];

  displayedColumns: {name: string, realName: string}[] = [];

  columnsToDisplay: string[] = this.displayedColumns
    .map((o) => {
      return o.name;
    })
    .slice();

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;
  @ViewChild(MatSort)
  sort!: MatSort;

  constructor(
    private spinner: NgxSpinnerService,
    private exchangeService: ExchangeService
  ) {
    this.dataSource = new MatTableDataSource(this.data);
  }

  async ngOnInit() {
    this.spinner.show();
    this.displayedColumns = [];
    this.data = [];
    this.data = (await this.exchangeService.list().toPromise()) ?? [];
    this.displayedColumns.push(
      {name: "currencyFrom", realName: 'Moneda Origen'},
      {name: "currencyTo", realName: 'Moneda Destino'},
      {name: "rate", realName: 'Tipo de Cambio'},
      {name: "amount", realName: 'Monto'},
      {name: "finalAmount", realName: 'Monto Final'},
      {name: "exchangeBy", realName: 'Cambio por'},
      {name: "exchangeDate", realName: 'Fecha'},
    );
    this.columnsToDisplay = this.displayedColumns
      .map((o) => {
        return o.name;
      })
      .slice();
    this.dataSource.data = this.data.map((o) => ({ ...o, exchangeDate: moment(o.exchangeDate).format("DD-MM-YYYY HH:MM") }));
    this.spinner.hide();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
}
