import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { Currency } from 'src/app/interfaces/currency';
import { CurrencyService } from 'src/app/services/currency/currency.service';
import { ExchangeRateService } from 'src/app/services/exchange-rate/exchange-rate.service';
import * as moment from 'moment';
import { ExchangeService } from 'src/app/services/exchange/exchange.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  public registerForm: FormGroup;
  public currencies: Currency[] = [];
  public rate: number = 0;
  public rateTime: string = "";
  public showResult: boolean = false;
  public rateResp: number = 0;
  public amount: number = 0;
  public finalAmount: number = 0;

  constructor(
    private formBuilder: FormBuilder,
    private currencyService: CurrencyService,
    private exchangeRateService: ExchangeRateService,
    private exchangeService: ExchangeService,
    private spinner: NgxSpinnerService
    ) {
      this.registerForm = this.formBuilder.group({
        currencyFrom: ['', Validators.required],
        currencyTo: ['', Validators.required],
        amount: [0, [Validators.required, Validators.min(0)]],
      });

    }

  async ngOnInit() {
    this.spinner.show();
    const currencies = await this.getCurrencies();
    this.currencies = currencies ?? [{ code: '', name: '', symbol: '' }];
    this.spinner.hide();
  }

  async getCurrencies () {
    return await this.currencyService.list().toPromise();
  }

  async search() {
    const { currencyFrom, currencyTo } = this.registerForm.value;
    if (!currencyFrom || !currencyTo) return;
    this.spinner.show();
    try {
      const exchangeRate = await this.exchangeRateService.find(currencyFrom, currencyTo).toPromise();
      this.rate = exchangeRate?.rate ?? 0;
      this.rateTime = exchangeRate ? moment(exchangeRate.exchangeDate).format("DD-MM-YYYY HH:MM") : '';
    } catch(error) {
      console.log(error);
      this.rate = 0;
      this.rateTime = 'No se encontro el tipo de cambio';
    }
    this.spinner.hide();
  }

  async register() {
    const { currencyFrom, currencyTo, amount } = this.registerForm.value;
    if (!currencyFrom || !currencyTo || !amount) return;

    this.spinner.show();
    try {
      const exchange = await this.exchangeService.create(currencyFrom, currencyTo, amount).toPromise();
      this.showResult = true;
      this.rateResp = exchange?.rate ?? 0;
      this.amount = exchange?.amount ?? 0;
      this.finalAmount = exchange?.finalAmount ?? 0;
    } catch(error) {
      console.log(error);
      this.rateResp = 0;
      this.amount = 0;
      this.finalAmount = 0;
    }
    this.spinner.hide();
  }

}
