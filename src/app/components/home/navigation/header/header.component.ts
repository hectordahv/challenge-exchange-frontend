import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @Output() public sidenavToggle = new EventEmitter();
  public header: string = '';

  constructor() { }

  ngOnInit() {
    this.header = localStorage.getItem('user') ?? "";
  }

  public onToggleSidenav = () => {
    this.sidenavToggle.emit();
  }

}
