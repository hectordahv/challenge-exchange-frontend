import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-sidenav-list',
  templateUrl: './sidenav-list.component.html',
  styleUrls: ['./sidenav-list.component.css'],
})
export class SidenavListComponent implements OnInit {
  @Output() sidenavClose = new EventEmitter();
  public displayedColumnsStatus: string[] = ['color', 'status'];
  public registerDisplay: string = 'block';
  public consultDisplay: string = 'block';
  public historialDisplay: string = 'none';

  constructor(
    private router: Router) {
    }

  async ngOnInit() {
    const roles = JSON.parse(localStorage.getItem('authorities') ?? "[]");
    const admin = roles.find((x: { authority: string }) => x.authority === 'ROLE_ADMIN');
    if (admin){
      this.historialDisplay = 'block';
    }
  }

  public onSidenavClose = (route: string) => {
    this.router.navigate([route]);
    this.sidenavClose.emit();
  };

  public async logout() {
    Swal.fire({
      title: 'Información',
      text: '¿Desea salir?',
      icon: 'info',
      showCancelButton: true,
      confirmButtonText: 'Sí',
      cancelButtonText: 'No'
    }).then(async (result) => {
      if (result.value) {
        this.out();
      }
    });
  }
  public out(){
    localStorage.clear();
    this.router.navigate(['/login']);
  }
}
