import { Component, OnInit } from '@angular/core';
import { MatDrawerMode } from '@angular/material/sidenav';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {
  public openedSideNav: boolean = false;
  public modeSideNav: MatDrawerMode = 'side';

  constructor() { }

  async ngOnInit() {
    await this.detectDevice();
  }

  async detectDevice(){
    const ua = navigator.userAgent;
    if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini|Mobile|mobile|CriOS/i.test(ua)){
      this.openedSideNav = false;
      this.modeSideNav = 'over';
    }else{
      this.openedSideNav = true;
      this.modeSideNav = 'side';
    }
  }

}
