import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavigationComponent } from './navigation/navigation.component';
import { HeaderComponent } from './navigation/header/header.component';
import { SidenavListComponent } from './navigation/sidenav-list/sidenav-list.component';
import { HomeRoutingModule } from './routing/home-routing.module';
import { CustomMaterialModule } from 'src/app/modules-core/material.module';
import { ExtraModule } from 'src/app/modules-core/extra.module';
import { RegisterComponent } from './register/register.component';
import { HistorialComponent } from './historial/historial.component';
import { ConsultComponent } from './consult/consult.component';

@NgModule({
  declarations: [
    NavigationComponent,
    HeaderComponent,
    SidenavListComponent,
    RegisterComponent,
    HistorialComponent,
    ConsultComponent
  ],
  entryComponents: [],
  imports: [CommonModule, CustomMaterialModule, ExtraModule, HomeRoutingModule],
})
export class HomeModule {}
