export interface ExchangeRate {
  currencyFrom: string;
  currencyTo: string;
  rate: number;
  exchangeDate: string;
}
