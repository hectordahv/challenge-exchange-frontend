export interface View {
  id?: number;
  name: string;
  display?: string;
}