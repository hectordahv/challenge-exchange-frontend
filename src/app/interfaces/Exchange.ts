export interface Exchange {
  currencyFrom: string;
  currencyTo: string;
  rate: number;
  amount: number;
  finalAmount: number;
  exchangeDate: string;
  exchangeBy: string;
}
